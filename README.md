Para ejecutar el código se debe importar el mismo (haciendo uso de git clone o las herramientas en disponibles en el entorno) al entorno de jupyter donde se ha desarrollado el Dataton.

Una vez ahí basta con ejecutar todas las celdas.

Requisito1:
Es necesario tener agregada la libreria de Google Earth Engine, para hacerlo en una máquina virtual en jupyter se utilizan las siguientes lineas de código:

python -m venv ~/venvs/mi_ambiente

realpath /env/lib/python3.10/site-packages > ~/venvs/mi_ambiente/lib/python3.10/site-packages/base_venv.pth

source ~/venvs/mi_ambiente/bin/activate

pip3 install earthengine-api

python -m ipykernel install --user --name=mi_ambiente

Nota: El nombre mi_ambiente puede ser cambiado por cualquier otro

Requisito2:
El kernel de jupyter debe estar seleccionado en el ambiente donde se encargo la librería de google earth engine.
